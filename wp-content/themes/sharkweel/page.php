<?php
//get_header();

while ( have_posts() ) : the_post(); ?>
    <?php

    $page = strtolower(str_replace(' ', '-', get_the_title()));

    $page = ($page == 'aktualności') ? 'aktualnosci': $page;
    $page = ($page == 'skłep') ? 'sklep': $page;

    $page = $page.".php";
    if ( locate_template( $page ) ) {
        include_once  $page;
    }


    ?>
<?php endwhile; //get_footer();?>
