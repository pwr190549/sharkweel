<!DOCTYPE html>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" >
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/lightbox.min.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/npm.js"></script>
    <?php //wp_head(); ?>
</head>

<body>

<div class="container-fluid" style=" padding: 0px;">
	<div class="row shop" >
		<div class="col-xs-12">
			<div class="shop_show_product col-xs-6">
				<img class="shop_arrows center-block" style="left:-10px; transform:scale(0.5,0.5)translate(0px, 355px);"src="<?php echo get_template_directory_uri(); ?>/img/prev.png">
				<img class="shop_arrows center-block" style="right:-10px; transform:scale(0.5,0.5)translate(0,355px);"src="<?php echo get_template_directory_uri(); ?>/img/next.png">
				
				<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/shop_kola1.png">
			</div>

			<div class="col-xs-6">
				<div class="col-xs-12">
					<h3>SIDEWINDER 70mm 78A</h3>
				</div>
				<div class="col-xs-12">
					<h3>329,99</h3>
				</div>
				<div class="col-xs-12">
					<br>
				</div>		
				<div class="shop_picker col-xs-6">
					<p>wybierz kolor <img src="<?php echo get_template_directory_uri(); ?>/img/next.png"></p>
				</div>		
				<div class="shop_picker col-xs-3">
					<p>twardość <img src="<?php echo get_template_directory_uri(); ?>/img/next.png"></p>
				</div>
				<div class="shop_picker col-xs-3">
					<p>ilość <img src="<?php echo get_template_directory_uri(); ?>/img/next.png"></p>
				</div>		
				<div class="col-xs-12">
					<br><br>
				</div>		
				<div class="shop_picker col-xs-12">
					<p>Opis <img style="transform: translateY(-5px) rotate(45deg) scale(0.7,0.7) ;" src="<?php echo get_template_directory_uri(); ?>/img/close.png"></p>
				</div>				
			</div>
		
		</div>
		<div class="shop_bottom col-xs-12">
			<div class="col-xs-6">
				<img class="shop_bottom_arrows center-block"style="left:-35px;"src="<?php echo get_template_directory_uri(); ?>/img/prev.png">
				<img class="shop_bottom_arrows center-block"style="right:-35px;"src="<?php echo get_template_directory_uri(); ?>/img/next.png">
				
				<div class="col-xs-3">
				<div class="shop_bottom_img col-xs-12">
					<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/shop_kola2.png">
				</div>
				</div>
				<div class="col-xs-3">
				<div class="shop_bottom_img col-xs-12">
					<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/shop_kola2.png">
				</div>
				</div>
				<div class="col-xs-3">
				<div class="shop_bottom_img col-xs-12">
					<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/shop_kola3.png">
				</div>
				</div>
				<div class="col-xs-3">
				<div class="shop_bottom_img col-xs-12">
					<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/shop_kola3.png">
				</div>
				</div>	
			</div>
			<div class="col-xs-6">
                <a href="#">
                    <p class=" text-uppercase col-xs-5 col-xs-offset-4">
                        Zamów
						<img style="position: absolute; right: 50px;" src="<?php echo get_template_directory_uri(); ?>/images/buy.png">
                    </p>
                </a>
            </div>
		</div>
	</div>
</div>
</body>