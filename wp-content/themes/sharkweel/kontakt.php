<div id="kontakt" class="row main-kontakt" style="font-family: CaviarDreams">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class=" center-block">
            <img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/lokalizacja-stopka.png">
            <p class="text-center" style="top:-130px; position:relative; color:white;">ul. Aleje Jerozolimskie 198F<br>lok. 4<br>Warszawa, Polska<br>
                <a href="https://www.google.pl/maps/place/Aleje+Jerozolimskie+198F,+Warszawa/@52.1970038,20.9269867,17z/data=!4m7!1m4!3m3!1s0x47
                1934ba98190147:0x59f1d703e3111096!2sAleje+Jerozolimskie+198F,+Warszawa!3b1!3m1!1s0x471934ba98190147:0x59f1d703e3111096">Zobacz mapę!</a>
            </p>
        </div>
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class=" center-block">
		<!--<a href="<?php /*echo get_home_url(); */?>/shop">
            <img class="center-block img-responsive" style="padding-top:30px;"src="<?php /*echo get_template_directory_uri(); */?>/images/zamow-juz-teraz.png">
            <p class="text-center" style="top:-85px; position:relative; color:white;text-transform: uppercase;font-weight: 700;}">zamów już teraz</p>
        </a>-->
            <a class="sklp">
                <img class="center-block img-responsive"  src="<?php echo get_template_directory_uri(); ?>/images/zamow-juz-teraz.png">
                <p class="text-center" style="top:-85px; position:relative; color:white;text-transform: uppercase;font-weight: 700;}">zamów już teraz</p>
            </a>
        </div>
    </div>

    <div class="col-md-4  col-sm-6 col-xs-12">
        <div class="center-block" >
            <img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/telefon-stopka.png">
            <p class="text-center" style="top:-100px; position:relative; color:white;"> 501 421 324 </p>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#dialog").dialog({
        autoOpen: false,
        width: 900,
        close: function(event, ui){
            $('#dialog').find('.admin-request-dialog').remove();
        },
        buttons: {
            "Zamknij": function() {
                $(this).dialog("close");
                $("#dialog p").remove();
            }
        }
    });

    $('.sklp').click(function(){
        $("#dialog p").remove();
        $("#dialog").dialog("option", "width", 300);
        $("#dialog").dialog("option", "title", "Informacja o sklepie");
        $("#dialog").dialog("open");
        var txt3 = document.createElement("p");
        txt3.innerHTML = "Dostępne wkrótce";
        $("#dialog").append(txt3);
        $("#dialog p").css("text-align", "center");
    });
</script>