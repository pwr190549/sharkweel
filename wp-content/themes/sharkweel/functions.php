<?php
function wptutsplus_register_theme_menu() {
    register_nav_menu( 'primary', 'Main Navigation Menu' );
}
add_action( 'init', 'wptutsplus_register_theme_menu' );

function gallery_slider ($images) {
    echo "<li>";
    echo "<div>";
    foreach($images as $img) {
        if($img){
            $full_img = str_replace('-150x150', '', $img);
            echo '';
            echo '<a href="'.$full_img.'" data-lightbox="image-1" data-title="Referencje">
                    <img class="galleryslider-img" src="'.$img.'" width=200 >
                 </a>';
        }
    }
    echo "</div>";
    echo "</li>";
}


function gallery_slider_video_photo ($files) {
    echo "<li>";
    echo "<div>";
    foreach($files as $file) {
        if($file){
            $file_src = (getimagesize($file) !== false) ? $file : get_home_url().'/wp-content/themes/sharkweel/images/wideo.png';
            $file = str_replace('-150x150', '', $file);
            echo '';
            echo '<a class="wplightbox" href="'.$file.'" >
                    <img class="galleryslider-img" src="'.$file_src.'" width=150 >
                 </a>';
        }
    }
    echo "</div>";
    echo "</li>";
}

function rand_merge($arr1, $arr2) {
    $res=array();
    $arr1=array_reverse($arr1);
    $arr2=array_reverse($arr2);
    foreach ($arr1 as $a1) {
        if (count($arr1)==0) {
            break;
        }
        array_push($res, array_pop($arr1));
        if (count($arr2)!=0) {
            array_push($res, array_pop($arr2));
        }
    }
    return array_merge($res, $arr2);
}

function catch_that_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];

    if(empty($first_img)){ //Defines a default image
        $first_img = get_template_directory_uri()."/images/default-news.png";
    }
    return $first_img;
}
function new_excerpt_length($length) {
    return 20;
}

?>