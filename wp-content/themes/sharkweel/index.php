<!DOCTYPE html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" >
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/lightbox.min.js"></script>
    <script src = "<?php echo get_template_directory_uri(); ?>/js/npm.js"></script>
    <?php wp_head(); ?>
</head>

<body>
<?php


$menu = wp_get_nav_menu_object ('main-menu');

$menu_items = wp_get_nav_menu_items($menu->term_id);

foreach ($menu_items as $menu_item) {
    $menupost=get_post($menu_item->object_id);

    $page = strtolower(str_replace(' ', '-', $menupost->post_title));

    $page = ($page == 'aktualności') ? 'aktualnosci': $page;
    $menu_pages[] = $page;

}
?>
<div id="page" class="container-fluid" style=" padding: 0px;">
        <div class="row header-class" >
            <div class = "navbar margin-bottom nav-bar-div-custom" style="">
                <button class = "navbar-toggle button-navbar nav-bar-button-custom" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                    <span class = "icon-bar"></span>
                    <span class = "icon-bar"></span>
                    <span class = "icon-bar"></span>
                </button>

                <div class = "collapse navbar-collapse navHeaderCollapse padding-r-50 clear-left">
				 
                    <ul class = "nav navbar-nav navbar-right navbar-ul" style="margin: 0px;">
						<li><img id ="logo-small" class="center-block img-responsive header-text hidden-xs"  style="left:50px; position:fixed; display:none;"src="<?php echo get_template_directory_uri(); ?>/images/logobialesharkwheel.png"></li>
                        <?php
                        foreach ($menu_items as $menu_item) {
                            $menupost=get_post($menu_item->object_id);
                            $page = strtolower(str_replace(' ', '-', $menupost->post_title));
                            if($page!= 'strona-główna') {
                                $page = ($page == 'aktualności') ? 'aktualnosci': $page;
                                if($page == 'sklep') { ?>
                                    <!--<li class = "active"><a class="anchor" href="<?php /*echo get_home_url().'/shop/' */?>"><?php /*echo $menupost->post_title; */?></a></li>-->
                                    <li class = "active sklep"><a class="anchor" ><?php echo $menupost->post_title; ?></a></li>
                                <?php }
                                else { ?>
                                    <li class = "active"><a class="anchor" href="<?php echo '#'.$page; ?>"><?php echo $menupost->post_title; ?></a></li>
                                <?php };
                            }
                            else { ?>
                                <li class = "active"><a href="<?php echo $menu_item->url; ?>"><?php echo $menupost->post_title; ?></a></li>
                            <?php
                            }
                        } ?>
                    </ul>
                </div>
            </div>
			
			<div class="col-xs-12" style="margin-top: 65px;">
				<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
			</div>

			<div id="margin-auto" class="col-xs-12 move"  >
                <a class="anchor" href="#opis">
					<p class="center-block">Perfect cube, perfect circle <br> and perfect sine wave
					</p>
                    <!--<img class="center-block img-responsive header-text" src="<?php echo get_template_directory_uri(); ?>/images/napis.png">-->
                </a>

			</div>
			<br><br><br><br>
			<div class="col-xs-12 move">
                <a class="anchor" href="#opis">
                    <img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/arrows.png">
                </a>
			</div>
			<br><br>
        </div>
        <div id="fix" style="width: 100%;background-color: #000000;"></div>
		<div id="opis" class="row shark-wheel paralelogram" style="">

			<div class="col-xs-12">
				<div class="col-xs-12 col-md-4 col-sm-4" style="padding-top:80px">
					<img class="center-block img-responsive img-circle darken-img" src="<?php echo get_template_directory_uri(); ?>/images/deskorolka1.jpg">
				</div>
				
				<div class="col-xs-12 col-md-8 col-sm-8">
					<div class="col-xs-12, center-block">
						<h2>Shark Wheel
						</h2>
						<!--<img class="center-block img-responsive main-img" src="<?php echo get_template_directory_uri(); ?>/images/napis1.png">-->
					</div>
					<div class="col-xs-12, center-block">
						<h3> Co to takiego?
						</h3>
						<!--<img class="center-block img-responsive main-img" src="<?php echo get_template_directory_uri(); ?>/images/napis2.png">-->
					</div>
                        <?php
                        $about_top = get_page_by_title('opis')->post_content;
                        $about_top = strip_tags($about_top);
                        ?>
					<p style="font-family: CaviarDreams">
                        <?php echo $about_top?>
					</p>
				</div>
			</div>
		</div>
			
		<div class="row about-tech paralelogram ">
			<div class="col-xs-12 col-md-3 col-sm-3">
				<img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/kolo_1.png">
			</div>
            <div>
                <p class=" col-md-6 col-sm-6 col-xs-12"  style="margin-top: 25px;font-family: CaviarDreams;font-weight: bold;">
                    <?php
                    $about_bottom = get_page_by_title('opis-white')->post_content;
                    $about_bottom = strip_tags($about_bottom);
                    echo $about_bottom;
                    ?>
                </p>
            </div>
			<div class="col-xs-12 col-md-3 col-sm-3 " > <!--hidden-xs hidden-sm col-sm-offset-1-->
				<img class="center-block img-responsive " src="<?php echo get_template_directory_uri(); ?>/images/kolo_2.png">
			</div>
		</div>

        <div  class="row main-order paralelogram">
            <div>
                <!--<a href="<?php /*echo get_home_url(); */?>/shop">
                    <p class=" text-uppercase text-center col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <img src="<?php /*echo get_template_directory_uri(); */?>/images/buy.png">
                        <span class="hidden-xs">Zamów już teraz!</span>
                    </p>
                </a>-->
                <a class="sklep">
                    <p class=" text-uppercase text-center col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/buy.png">
                        <span class="hidden-xs">Zamów już teraz!</span>
                    </p>
                </a>
            </div>
        </div>
    </div>
    <div id="dialog"></div>
	<div id="loading"></div>
    <script type="application/javascript">


		function show(id, value) {
			document.getElementById(id).style.display = value ? 'block' : 'none';
		}

        $('.anchor').click(function(){
            $('html, body').animate({
                scrollTop: $( $(this).attr('href') ).offset().top
            }, 700);
            return false;
        });

        $(window).load(function() {
            var height = $( window ).height();
            var width = $( window ).width();
            var a = 0.044*width;
            $('.header-class').css("height", height);
            $('#margin-auto').css("margin-top", height*0.25);
            $('#fix').css ("height", a);
            $('#fix').css ("margin-bottom", a*-1);
            $('#opis').css ("margin-top", a/2);
			if(width < 400){
				$('.header-class p').css("font-size", 18);
			}

			show('page', true);
			show('page2', true);
			show('kontakt', true);
			show('loading', false);
        });

        $("#dialog").dialog({
            autoOpen: false,
            //width: auto,
            close: function(event, ui){
                $('#dialog').find('.admin-request-dialog').remove();
            },
            buttons: {
                "Zamknij": function() {
                    $(this).dialog("close");
                    $("#dialog p").remove();
                }
            }
        });

		$('.sklep').click(function(){
            $("#dialog p").remove();
            $("#dialog").dialog("option", "width", 300);
            $("#dialog").dialog("option", "title", "Informacja o sklepie");
            $("#dialog").dialog("open");
            var txt3 = document.createElement("p");
            txt3.innerHTML = "Dostępne wkrótce";
            $("#dialog").append(txt3);
            $("#dialog p").css("text-align", "center");
        });

		 $(document).on("scroll",function(e){
            e.stopPropagation();
            if ($(document).scrollTop() > 500) {
					document.getElementById("logo-small").style.display = "initial";
                }

            else {
                document.getElementById("logo-small").style.display = "none";
            }
         });

        document.getElementById("aktualnosci").style.display = 'none';
        //$('#aktualnosci').css("display", "none");



    ;
    </script>
    <?php
    foreach ($menu_pages as $p) {
        $include = $p.".php";
        if ($include != 'sklep.php') {
            if ( locate_template( $include ) ) {
                include_once  $include;
            }
        }
    }

    wp_footer();
    ?>

</body>