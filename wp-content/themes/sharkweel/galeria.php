

<div id="page2" class="row main-aktualnosci paralelogram">
    <div id="aktualnosci" class="col-md-6 col-xs-12 main-aktualnosci-text">
        <div style="transform: rotateY(2.5deg);">
            <?php
            $id = get_page_by_title('aktualności')->ID;
            $post = get_post($id);
            setup_postdata( $post);
            ?>
            <?php the_content(); ?>
            <hr>
        </div>
        <!--<div class="main-aktualnosci-text" style="overflow: hidden">
            <hr>
            <div class="col-xs-12 col-sm-5 col-md-5 main-aktualnosci-img">
                <img class="center-block img-responsive img-circle " src="<?php /*echo get_template_directory_uri(); */?>/images/deskorolka1.png">
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7">
                <h3>Tytuł</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                    ullamcorper maximus metus, ut eleifend metus laoreet quis.
                </p>
                <hr>
            </div>
        </div>-->
    </div>

    <div id="galeria" class="col-md-6 col-xs-12" style="margin-bottom: -85px;">
        <?php
        $images = get_post_gallery_images(get_page_by_title('galeria'));
        preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', get_page_by_title('galeria')->post_content, $result, PREG_PATTERN_ORDER);
        $result = $result[0];
        $images = rand_merge($images, $result);
        if ($images) {?>
            <div class="galleryslider-div flexslider col-xs-11">
                <ul class="slides">
                    <?php
                    $counter = 0;
                    $count_img_page = 8;
                    $size_for = ceil(sizeof($images)/$count_img_page);
                    for ($i=0; $i<$size_for; $i=$i+1) {
                        for ($j = $counter; $j < $counter + $count_img_page; $j=$j+1){
                            $array[] = $images[$j];
                        }
                        gallery_slider_video_photo($array);
                        unset($array);
                        $counter = $counter + $count_img_page;
                    }?>
                </ul>
            </div>
        <?php
        }
        ?>
        <div class="col-xs-12" style="text-align: center; padding: 0px 15px 75px 15px;">
            <a href="https://youtu.be/WYCpdLbOpDs">
                <img width="250" src="<?php echo get_template_directory_uri(); ?>/images/wideo.png">
                <p style="margin-top: 20px;font-size: 23px">SharkWheel Amazing Deals!</p>
            </a>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(window).load(function() {
        $('.flexslider').flexslider();
    });

</script>