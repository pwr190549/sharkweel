<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy i ABSPATH. Więcej informacji
 * znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'sharkweel');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'sharkweel');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'sharkweel');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<-#kiY/99-M79<MLWp^HC{_X*kc6kp{/4^T]mKtbb;f|ZZ*NFE[`5G`7g9,YmJ#l');
define('SECURE_AUTH_KEY',  '[hxMuHwBIPo4#cV[r)i-iQbwt[F*%+vuYw>_0b`9[EN&y <.~m+Kg]qh6-=dPR8d');
define('LOGGED_IN_KEY',    'fJ;:jy Shg:+~J?6p`S8tW9-x>H|wF1+?IebX~`Y2)~hdOQ#hLJECC{*mqTZl&@x');
define('NONCE_KEY',        '5a[L9Iif7Ae(C;7gLI{e|fH1@F3P._DpB`l!i[z&^ 5?}&ub7SC4,RmeHlUfFXQ6');
define('AUTH_SALT',        'Lbx Eou`Om-cD%ZkOW4<^0O-K6 T+idA-P0VrB6<t++X6cU1!v2mG4hi-+=luzWY');
define('SECURE_AUTH_SALT', '~f)Z=#>X?Ho6w7|N#9_FO{|5Z-p[p1F(3{^AX_(yjU+MgNYsq:/H@V~M y*ZfG$m');
define('LOGGED_IN_SALT',   'a^+;S+W((%q%X:i_UvI:>!J~vqF<K-W{w(Ic35l *WA6f^`vq`=jD&uh[ZKo?)VP');
define('NONCE_SALT',       'TlkP^>+}vJ}qDpy M~p)>+hmvl5}jy!&:`JOe2A^C|M;7sSpsq8-]kH-v6)-pt6z');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
